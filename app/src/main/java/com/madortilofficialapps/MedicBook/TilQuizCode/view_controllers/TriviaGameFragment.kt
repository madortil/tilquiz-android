package com.madortilofficialapps.MedicBook.TilQuizCode.view_controllers

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.madortilofficialapps.MedicBook.R
import com.madortilofficialapps.MedicBook.TilQuizCode.model.*
import com.madortilofficialapps.MedicBook.TilQuizCode.model.TriviaQuestion.Topic.*
import com.madortilofficialapps.MedicBook.TilQuizCode.views.TriviaAnswerCard
import com.madortilofficialapps.MedicBook.TilVideo.Controller.MenuViewController
import com.madortilofficialapps.MedicBook.extensions.correspondingBubbleIDForTopic
import kotlinx.android.synthetic.main.fragment_trivia_game.*
import kotlinx.android.synthetic.main.game_bar.view.*
import kotlinx.android.synthetic.main.game_menu.view.*
import kotlinx.android.synthetic.main.game_overview.view.*
import org.jetbrains.anko.bundleOf
import org.jetbrains.anko.toast
import java.util.*
import kotlin.concurrent.schedule

/**
 * A simple [Fragment] subclass.
 *
 */
class TriviaGameFragment : Fragment(), GameDelegate {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_trivia_game, container, false)
    }
    private var answerCards: List<TriviaAnswerCard> = emptyList()

    private var game: TriviaGame? = null
    private var topics: List<TriviaQuestion.Topic>? = null
    private var questionsAnswered : MutableList<SummaryQuestion> = mutableListOf<SummaryQuestion>()
    private var questionNumber = 0

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        topics = listOf(MenuViewController.topicChosen)

        try {
            game = TriviaGame(context!!, topics!!)
        } catch (e: Exception) {
            activity?.toast(e.localizedMessage)
            findNavController().popBackStack()
            return
        }
        setupDisplay()

        game!!.delegate = this
        game!!.start()
    }

    private fun setupDisplay() {
        // Game Bar
        gameBar?.myScoreTextView?.text = "0/0"
        gameBar.setupLayout(GameSession.GameType.trivia)
        gameBar?.menuImageButton?.setOnClickListener { gameMenu.isVisible = true }
        // Setup answerCards click listeners
        answerCards = listOf(answer1Button, answer2Button, answer3Button, answer4Button)
        answerCards.forEach { answerCard ->
            answerCard.setOnClickListener {
                answerButtonClicked(answerCard)
            }
        }

        gameBar?.opponentScoreTextView?.isInvisible = true



        // Setup answerCards needed traits for future animations
        for (index in answerCards.indices) {
            answerCards[index].activity = activity
            answerCards[index].originalXOrigin = answerCards[index].x
            answerCards[index].originalYOrigin = answerCards[index].y
            // Set the Right side of cards to lean Left (default is Right)
            if (index%2 == 1) {
                answerCards[index].cardLeanRotation = TriviaAnswerCard.CardLeanRotation.Left
            }
        }
        // Starting deck position TODO: Get it right
        for (index in answerCards.indices) {
            if (index%2 == 0) {
                answerCards[index].rotation = -90F
            } else {
                answerCards[index].rotation = 90F
            }
        }

        // Game Menu
        gameMenu.exitGameButton.setOnClickListener {
            game?.cleanUp()
            gameOverview.setupLayout(game?.myPlayerData?.score, questionNumber)
            gameOverview.isVisible = true
            gameMenu.isVisible = false
        }
        gameMenu.returnToGameButton.setOnClickListener {
            gameMenu.isVisible = false
        }

        // Game Overview
        gameOverview.continueButton.setOnClickListener {
            findNavController().popBackStack()
        }
        gameOverview.summaryButton.setOnClickListener {
            showSummary()
        }
        Log.d("QuestionsTest", "End of setup display")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        game?.cleanUp()
    }

    // MARK: - Game Flow

    private fun displayTriviaQuestion(animationCompleted: (() -> Unit)? = null) {
        if (!gameBar.opponentPlayerAnimationDrawable.isRunning) {
            gameBar.opponentPlayerAnimationDrawable.start()
        }
        questionTextView.text = game!!.library.currentQuestion
        if (game?.library?.currentTopic != null) {
            questionTopicBubbleImageView.setImageResource(TriviaQuestion.Topic.correspondingBubbleIDForTopic(
                    game!!.library.currentTopic))
        }
        val cImageResources = cardsImageResourcesForTopic(game?.library?.currentTopic)
        for (index in answerCards.indices) {
            answerCards[index].text = game!!.library.currentAnswers[index]
            answerCards[index].setBackgroundResource(cImageResources[index])
        }

        Timer().schedule(resources.getInteger(R.integer.triviaQuestionDisplayedDelay).toLong()) {
            activity?.runOnUiThread {
                answerCards.forEach {
                    it.isEnabled = true
                    it.alpha = 1F
                }
            }
            answerCards[0].animateToOriginalPosition()
            answerCards[1].animateToOriginalPosition()
            answerCards[2].animateToOriginalPosition()
            answerCards[3].animateToOriginalPosition {
                animationCompleted?.invoke()
            }
        }
    }
    private fun cardsImageResourcesForTopic(topic: TriviaQuestion.Topic?): List<Int> {
        return when (topic) {
            anatomy, medicine, teamWork, mentalHealth ->
                listOf(R.drawable.trivia_answer_1b, R.drawable.trivia_answer_2b,
                        R.drawable.trivia_answer_3b, R.drawable.trivia_answer_4b)
            cpr, anamnesis, publicHealth ->
                listOf(R.drawable.trivia_answer_1r, R.drawable.trivia_answer_2r,
                        R.drawable.trivia_answer_3r, R.drawable.trivia_answer_4r)
            else ->
                listOf(R.drawable.trivia_answer_1g, R.drawable.trivia_answer_2g,
                        R.drawable.trivia_answer_3g, R.drawable.trivia_answer_4g)
        }
    }

    private fun answerButtonClicked(sender: TriviaAnswerCard?) {
        questionNumber++
        answerCards.forEach {
            it.isEnabled = false
            it.alpha = 0.8F
        }
        if (sender != null) {
            questionsAnswered.add(SummaryQuestion(TriviaQuestion(game!!.library.currentQuestion, game!!.library.currentAnswers, game!!.library.currentCorrectAnswer, game!!.library.currentTopic), answerCards.indexOf(sender)))
            if (answerCards.indexOf(sender) == game!!.library.currentCorrectAnswer) {
                correctAnswerClicked(sender)
            } else {
                incorrectAnswerClicked(sender)
            }
        } else incorrectAnswerClicked(null)
    }

    private fun correctAnswerClicked(sender: TriviaAnswerCard) {
        game!!.correctAnswerPicked()
        sender.animateCorrectAnswer {
            answerCards[0].animateCorrectAnswerPicked()
            answerCards[1].animateCorrectAnswerPicked()
            answerCards[2].animateCorrectAnswerPicked()
            answerCards[3].animateCorrectAnswerPicked {
                game!!.readyForNextQuestion()
                if(game!!.myPlayerData!!.gameState == GameSession.PlayerData.State.complete){
                    //findNavController().popBackStack()
                    gameComplete()
                }
            }
        }
    }
    private fun incorrectAnswerClicked(sender: TriviaAnswerCard?) {
        game!!.incorrectAnswerPicked()
        gameBar?.myScoreTextView?.text = "" + game?.myPlayerData?.score.toString() + "/" + questionNumber
        sender?.animateIncorrectAnswer()
        answerCards[game!!.library.currentCorrectAnswer].animateCorrectAnswer {
            answerCards[0].animateIncorrectAnswerPicked()
            answerCards[1].animateIncorrectAnswerPicked()
            answerCards[2].animateIncorrectAnswerPicked()
            answerCards[3].animateIncorrectAnswerPicked {
                game!!.readyForNextQuestion()
                if(game!!.myPlayerData!!.gameState == GameSession.PlayerData.State.complete){
                   // findNavController().popBackStack()
                    gameComplete()
                }
            }
        }
    }

    override fun gameNextQuestionReady() {
        activity?.runOnUiThread {
            displayTriviaQuestion()
        }
    }
    override fun gameMyScoreUpdated() {
       if(game?.myPlayerData?.score.toString() != null){
           activity?.runOnUiThread { gameBar?.myScoreTextView?.text = "" + game?.myPlayerData?.score.toString() + "/" + questionNumber
           }
       }else{
           activity?.runOnUiThread { gameBar?.myScoreTextView?.text = "0/0"
           }
       }

    }
  //  override fun gameTimeElapsedUpdated() {
     //   activity?.runOnUiThread { gameBar?.timeElapsed = game?.timeElapsed ?: 0.0 }
   // }
  //  override fun gameOutOfTime() {
    //    activity?.runOnUiThread {
    //        incorrectAnswerClicked(null)
     //   }
  //  }
    override fun gameEnded() {
        game?.cleanUp()
        if (game?.myPlayerData?.score != null) {
            gameOverview.setupLayout(
                    game!!.myPlayerData!!.score, questionNumber)
        } else {
            gameOverview.setupLayout(game?.myPlayerData?.score, questionNumber)
        }
        gameOverview.isVisible = true
    }
    override fun gameComplete() {
        game?.cleanUp()
        if (game?.myPlayerData?.score != null) {
            gameOverview.setupLayout(
                    game!!.myPlayerData!!.score, questionNumber)
        } else {
            gameOverview.setupLayout(game?.myPlayerData?.score, questionNumber)
        }
        gameOverview.isVisible = true
    }
    fun showSummary(){
        if(questionsAnswered.size <= 0){
            findNavController().popBackStack()
        }else{
            var triviaQuestionsAnsweredBundle = bundleOf("questions" to questionsAnswered)
            findNavController().navigate(R.id.action_triviaGameFragment_to_triviaSummaryFragment, triviaQuestionsAnsweredBundle)
        }
    }
}
