package com.madortilofficialapps.MedicBook.TilQuizCode.model

interface DatabaseFacadeDelegate {
    fun triviaQuestionsChanged() {}
    fun gameSessionsChanged() {}
    fun currentGameSessionChanged() {}
    fun opponentGameSessionDataChanged() {}
    fun scoreboardChanged() {}
    fun myScoreChanged() {}
}