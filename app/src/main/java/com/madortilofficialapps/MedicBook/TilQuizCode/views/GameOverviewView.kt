package com.madortilofficialapps.MedicBook.TilQuizCode.views

import android.content.Context
import android.graphics.drawable.AnimationDrawable
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.madortilofficialapps.MedicBook.R
import kotlinx.android.synthetic.main.game_overview.view.*

class GameOverviewView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
    : ConstraintLayout(context, attrs, defStyleAttr) {

    init {
        LayoutInflater.from(context).inflate(R.layout.game_overview, this, true)
    }

    fun setupLayout( scoreAppended: Int?, questionsAnswered: Int) {
        myScoreReviewTextView.text = "0"
        if (scoreAppended != null) {
            myScoreReviewTextView.text = scoreAppended.toString()
            if(scoreAppended > questionsAnswered / 2 && questionsAnswered != 0) {
                didWinTextView.text = resources.getString(R.string.did_win_text_view_singleplayer)
                if(scoreAppended == 1){
                    scoreWonTextView.text = "צדקת בשאלה אחת"
                }else{
                    scoreWonTextView.text = "ֿצדקת ב-" + scoreAppended + " שאלות"
                }
                kualaImageView.setImageResource(R.drawable.kuala_happy)
            }else if(questionsAnswered == 0){
                didWinTextView.text = "מפחד לטעות?!"
                scoreWonTextView.text = "לא ענית על אף שאלה"
                kualaImageView.setImageResource(R.drawable.kuala_sad)
            }else if(scoreAppended == 1){
                didWinTextView.text = resources.getString(R.string.did_win_text_view_quit)
                scoreWonTextView.text = "צדקת בשאלה אחת"
                kualaImageView.setImageResource(R.drawable.kuala_sad)
            }else {
                didWinTextView.text = resources.getString(R.string.did_win_text_view_quit)
                scoreWonTextView.text = "צדקת ב-" + scoreAppended + " שאלות"
                kualaImageView.setImageResource(R.drawable.kuala_sad)
            }

        } else {
            didWinTextView.text = resources.getString(R.string.did_win_text_view_quit)
            scoreWonTextView.text = ""
            kualaImageView.setImageResource(R.drawable.kuala_sad)
        }


        (myScoreReviewTextView.background as AnimationDrawable).start()
    }
}