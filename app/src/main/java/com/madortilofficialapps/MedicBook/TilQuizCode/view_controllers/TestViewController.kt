package com.madortilofficialapps.MedicBook.TilQuizCode.view_controllers

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.madortilofficialapps.MedicBook.R
import com.madortilofficialapps.MedicBook.TilVideo.Controller.MenuViewController
import kotlinx.android.synthetic.main.fragment_start_test.*

class TestViewController : Fragment(){
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_start_test, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val parent = parentFragment as MenuViewController
        super.onViewCreated(view, savedInstanceState)
        startTestButton.setOnClickListener {
            parent.startTrivia()
        }
    }
}
