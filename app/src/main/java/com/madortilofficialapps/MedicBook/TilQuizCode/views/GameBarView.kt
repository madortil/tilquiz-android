package com.madortilofficialapps.MedicBook.TilQuizCode.views

import android.content.Context
import android.graphics.drawable.AnimationDrawable
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.madortilofficialapps.MedicBook.R
import com.madortilofficialapps.MedicBook.TilQuizCode.model.GameSession
import kotlinx.android.synthetic.main.game_bar.view.*

class GameBarView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
    : ConstraintLayout(context, attrs, defStyleAttr) {

    init {
        LayoutInflater.from(context).inflate(R.layout.game_bar, this, true)
    }

    lateinit var opponentPlayerAnimationDrawable: AnimationDrawable

    fun setupLayout(gameType: GameSession.GameType) {

        (myPlayerImageView.drawable as AnimationDrawable).start()

        opponentPlayerAnimationDrawable = (opponentPlayerImageView.drawable as AnimationDrawable)
        opponentPlayerImageView.scaleX = -1F
        opponentPlayerAnimationDrawable.start()
    }
}