package com.madortilofficialapps.MedicBook.TilQuizCode.model

import android.util.Log
import androidx.room.*
import java.io.Serializable

@Entity(tableName = "trivia_question_table")
@TypeConverters(TriviaQuestion.AnswersConverters::class, TriviaQuestion.Topic.TopicConverters::class)
data class TriviaQuestion(
        @PrimaryKey var question: String = "",
        var answers: List<String> = emptyList(),
        var correctAnswer: Int = 0,
        var topic: Topic = Topic.anatomy)
{
    @Ignore constructor(): this("", emptyList(),0, Topic.anatomy)
    fun mixAnswers(){
        Log.d("Random", "Random happening, answer is: " + correctAnswer)
        val correctAnswerString = answers[correctAnswer]
        answers = answers.shuffled()
        answers = answers.shuffled()
        answers = answers.shuffled()
        //Log.d("Random", "Question is " + question)
      //  Log.d("Random", "Answers are " + answers.toString())
      //  Log.d("Random", "Correct answer is " + correctAnswer)
      //  Log.d("Random", "Correct answer is " + answers[correctAnswer])

        for(index in 0..answers.count() - 1){
            //Log.d("Random", index.toString())
            if(answers[index] == correctAnswerString){
                correctAnswer = index
            }
        }
        Log.d("Random", "Random happened, new answer is: " + correctAnswer)
    }
    class AnswersConverters {
        @TypeConverter
        fun stringToAnswersList(answersString: String): List<String> {
            return answersString.split(",").map { it.trim() }
        }
        @TypeConverter
        fun answersListToString(answers: List<String>): String {
            return answers.joinToString()
        }
    }
    enum class Topic {
        anatomy,
        cpr,
        routine,
        medicine,
        teamWork,
        anamnesis,
        trauma,
        mentalHealth,
        publicHealth,
        nbc,
        other;

        companion object

        class TopicConverters {
            @TypeConverter fun fromStringToTopic(topicString: String) : Topic {
                return Topic.valueOf(topicString)
            }
            @TypeConverter fun fromTopicToString(topic: Topic) : String {
                return topic.toString()
            }
        }
    }
}
data class SummaryQuestion(var question: TriviaQuestion, var selectedAnswer: Int) : Serializable {
}