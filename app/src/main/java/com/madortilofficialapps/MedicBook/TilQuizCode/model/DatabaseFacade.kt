package com.madortilofficialapps.MedicBook.TilQuizCode.model

import android.content.Context
import androidx.room.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import org.jetbrains.anko.async

object DatabaseFacade {

    var delegate: DatabaseFacadeDelegate? = null

    private val mAuth = FirebaseAuth.getInstance()


    // MARK: - Database

    private val databaseReference = FirebaseDatabase.getInstance().reference
    private var dao = AppRoomDatabase.getDatabase()?.dao()


    // MARK: - Trivia

    /**
     * This clients Trivia Questions. Gets stored locally but updates from the database
     */
    var triviaQuestions: List<TriviaQuestion>? = null
        private set(triviaQuestions) {
            field = triviaQuestions
            delegate?.triviaQuestionsChanged()
        }

    fun fetchTriviaQuestions() {
        val triviaQuestionsListener = object : ValueEventListener {
            override fun onDataChange(triviaQuestionsDataSnapshot: DataSnapshot) {
                triviaQuestions = getListOf(triviaQuestionsDataSnapshot)
                async { dao?.updateTriviaQuestions(triviaQuestions!!) }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Fetching Trivia failed
            }
        }
        async { triviaQuestions = dao?.loadTriviaQuestions() }
        databaseReference.child("TriviaQuestions").addListenerForSingleValueEvent(triviaQuestionsListener)
    }

    // MARK: - Convenience

    inline fun <reified T: Any> getListOf(dataSnapshot: DataSnapshot): List<T> {
        val list = mutableListOf<T>()
        dataSnapshot.children.toMutableList().mapNotNullTo(list) {
            try {
                it.getValue<T>(T::class.java)
            } catch (e: Exception) {
                return mutableListOf()
            }
        }
        return list
    }
}

// MARK: - Room database

abstract class AppRoomDatabase : RoomDatabase() {
    abstract fun dao() : AppRoomDatabaseDao

    companion object {
        @Volatile
        private var shared: AppRoomDatabase? = null

        fun getDatabase(applicationContext: Context? = null): AppRoomDatabase? {
            if (shared == null && applicationContext != null) {
                synchronized(AppRoomDatabase::class.java) {
                    if (shared == null) {
                        shared = Room.databaseBuilder(applicationContext,
                                AppRoomDatabase::class.java, "app_room_database")
                                .build()
                    }
                }
            }
            return shared
        }
    }
}

@Dao
interface AppRoomDatabaseDao {

    @Transaction
    fun updateTriviaQuestions(triviaQuestions: List<TriviaQuestion>) {
        deleteAllTriviaQuestions()
        insertTriviaQuestions(triviaQuestions)
    }

    @Insert
    fun insertTriviaQuestions(triviaQuestions: List<TriviaQuestion>)

    @Query("DELETE FROM trivia_question_table")
    fun deleteAllTriviaQuestions()

    @Query("SELECT * FROM trivia_question_table")
    fun loadTriviaQuestions(): List<TriviaQuestion>



}

