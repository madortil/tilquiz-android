package com.madortilofficialapps.MedicBook.TilQuizCode.model

data class Score(var displayName: String = "",
                 var score: Int = 0)
