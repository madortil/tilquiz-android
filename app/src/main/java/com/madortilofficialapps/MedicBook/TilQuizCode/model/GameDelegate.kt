package com.madortilofficialapps.MedicBook.TilQuizCode.model

interface GameDelegate {
    fun gameNextQuestionReady() {}
    fun gameOutOfTime() {}
    fun gameEnded() {}
    fun gameComplete() {}
    fun gameMyScoreUpdated() {}
    fun gameTimeElapsedUpdated() {}
    fun gameOpponentPresent() {}
}