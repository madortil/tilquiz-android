package com.madortilofficialapps.MedicBook.TilQuizCode.view_controllers
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.madortilofficialapps.MedicBook.R
import com.madortilofficialapps.MedicBook.TilQuizCode.model.SummaryQuestion
import kotlinx.android.synthetic.main.fragment_trivia_summary.*

class TriviaSummaryFragment : Fragment(){
    var currentQuestion = 0
    var questions : MutableList<SummaryQuestion> = mutableListOf<SummaryQuestion>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_trivia_summary, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        questions = arguments!!.getSerializable("questions") as MutableList<SummaryQuestion>
        currentQuestion = 0
        nextButton.setOnClickListener { view ->
            nextQuestion()
        }
        previousButton.setOnClickListener { view ->
            previousQuestion()
        }
        homeButton.setOnClickListener { view ->
            backToMenu()
        }
        if(questions != null && questions.size > 0){
            showQuestions()
        }else{
            backToMenu()
        }
    }
    fun nextQuestion(){
        if(currentQuestion < (questions.size - 1)){
            currentQuestion++
            showQuestions()
        }else{
            backToMenu()
        }
    }
    fun previousQuestion(){
        if(currentQuestion > 0){
            currentQuestion--
            showQuestions()
        }
    }
    fun showQuestions(){ //show the current question, and mark the correct and selected answer

        //setup the current screen
        questionNumberTextView.text = "" + (currentQuestion + 1) + "/" + questions.count()
        answer1Button.setBackgroundResource(R.drawable.trivia_answer_1b)
        answer1Button.text = questions[currentQuestion].question.answers[0]

        answer2Button.setBackgroundResource(R.drawable.trivia_answer_2b)
        answer2Button.text = questions[currentQuestion].question.answers[1]

        answer3Button.setBackgroundResource(R.drawable.trivia_answer_3b)
        answer3Button.text = questions[currentQuestion].question.answers[2]

        answer4Button.setBackgroundResource(R.drawable.trivia_answer_4b)
        answer4Button.text = questions[currentQuestion].question.answers[3]

        questionTextView.text = questions[currentQuestion].question.question

        //mark selected answer as red if it was wrong
        if(questions[currentQuestion].selectedAnswer != questions[currentQuestion].question.correctAnswer){
            when(questions[currentQuestion].selectedAnswer){
                0 ->{
                    answer1Button.setBackgroundResource(R.drawable.trivia_answer_1r)
                }
                1 ->{
                    answer2Button.setBackgroundResource(R.drawable.trivia_answer_2r)
                }
                2 ->{
                    answer3Button.setBackgroundResource(R.drawable.trivia_answer_3r)
                }
                3 ->{
                    answer4Button.setBackgroundResource(R.drawable.trivia_answer_4r)
                }
            }
        }

        //mark the correct answer as green
        when(questions[currentQuestion].question.correctAnswer){
            0 ->{
                answer1Button.setBackgroundResource(R.drawable.trivia_answer_1g)
            }
            1 ->{
                answer2Button.setBackgroundResource(R.drawable.trivia_answer_2g)
            }
            2 ->{
                answer3Button.setBackgroundResource(R.drawable.trivia_answer_3g)
            }
            3 ->{
                answer4Button.setBackgroundResource(R.drawable.trivia_answer_4g)
            }
        }
    }

    fun backToMenu(){
        findNavController().popBackStack()
        findNavController().popBackStack()
    }
}