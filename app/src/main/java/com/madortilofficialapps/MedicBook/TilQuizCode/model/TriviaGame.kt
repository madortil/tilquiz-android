package com.madortilofficialapps.MedicBook.TilQuizCode.model

import android.content.Context
import com.madortilofficialapps.MedicBook.R
import com.madortilofficialapps.MedicBook.TilQuizCode.model.GameSession.PlayerData.State.complete
import com.madortilofficialapps.MedicBook.extensions.random

class TriviaGame(context: Context, topics: List<TriviaQuestion.Topic>) : Game() {

    val library: TriviaLibrary
    private var maxQuestions = context.resources.getInteger(R.integer.triviaMaxQuestions)
    init {
        if (!DatabaseFacade.triviaQuestions.isNullOrEmpty()) {
            this.library = TriviaLibrary(DatabaseFacade.triviaQuestions, topics)
        } else {
            DatabaseFacade.fetchTriviaQuestions()
            throw Exception("שׁאלות לא נטענו")
        }
        if(maxQuestions > library.library.size){
            maxQuestions = library.library.size - 1
        }
    }



    private var pickedQuestionIndices: MutableList<Int> = mutableListOf()
    private fun pickRandomQuestion() {
        var index = library.library.size.random()
        // May become easily infinite
        while (pickedQuestionIndices.contains(index)) {
            index = library.library.size.random()
        }
        pickedQuestionIndices.add(index)
        library.currentLibraryQuestion = index
        library.shuffleAnswers()
    }

    override fun readyForNextQuestion() {
        // All the logic happens in the didSet of the myPlayerData var
        if (myPlayerData != null) {
            val temp = myPlayerData?.clone()
            temp!!.progression += 1
            myPlayerData = temp
        }
    }
    override fun correctAnswerPicked() {
        if (myPlayerData != null) {
            val temp = myPlayerData?.clone()
            temp!!.score += 1
            myPlayerData = temp
        }
    }
    override fun incorrectAnswerPicked() {
    }

    override var minScore: Int = context.resources.getInteger(R.integer.triviaMinScore)

    override var maxScore: Int = context.resources.getInteger(R.integer.triviaMaxScore)


    // MARK: - Game Session Changes Listeners

    override var myPlayerData: GameSession.PlayerData? = null
        set(d) {
            val oldValue = myPlayerData?.clone()
            field = d
            // Responds to my progression changes
            if (oldValue?.progression != myPlayerData?.progression) {
                if (myPlayerData?.progression == maxQuestions + 1) {
                    val temp = myPlayerData?.clone()
                    temp?.gameState = complete
                    myPlayerData = temp
                } else {
                    pickRandomQuestion()
                    delegate?.gameNextQuestionReady()
                }
            }
            myPlayerDataDidSet(oldValue)
        }
}