package com.madortilofficialapps.MedicBook.TilQuizCode.model

import com.madortilofficialapps.MedicBook.TilQuizCode.model.GameSession.PlayerData.State.*

abstract class Game: DatabaseFacadeDelegate {

    var delegate: GameDelegate? = null

    fun start() {
            myPlayerData = GameSession.PlayerData("Singleplayer", 0, 0, ongoing)
    }

    // MARK: - Game Flow

    abstract fun readyForNextQuestion()
    abstract fun correctAnswerPicked()
    abstract fun incorrectAnswerPicked()

    abstract var minScore: Int
    abstract var maxScore: Int


    // MARK: - Game Session Changes Listeners

    abstract var myPlayerData: GameSession.PlayerData?
    fun myPlayerDataDidSet(oldValue: GameSession.PlayerData?) {
        // Responds to my game state changes
        if (oldValue?.gameState != myPlayerData?.gameState && myPlayerData?.gameState != null) {
            respondToGameStateChanges(myPlayerData!!.gameState)
        }
        // Responds to my score changes
        if (oldValue?.score != myPlayerData?.score) {
            delegate?.gameMyScoreUpdated()
        }
        // Uploads changes to database if needed
    }
    private fun respondToGameStateChanges(state: GameSession.PlayerData.State) {
        when (state) {
            ended -> {
                delegate?.gameEnded()
            }
            complete -> {
                // Will add score whether a singleplayer or a multiplayer game was complete
                delegate?.gameComplete()
            }
            ongoing -> {
                delegate?.gameOpponentPresent()
            }
            else -> {}
        }
    }
    fun cleanUp() {
        delegate = null
    }
}