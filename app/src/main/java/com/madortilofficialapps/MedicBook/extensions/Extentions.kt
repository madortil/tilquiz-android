package com.madortilofficialapps.MedicBook.extensions

import android.animation.AnimatorInflater
import android.animation.AnimatorSet
import android.util.Patterns
import android.view.View
import com.madortilofficialapps.MedicBook.R
import com.madortilofficialapps.MedicBook.TilQuizCode.model.TriviaQuestion
import com.madortilofficialapps.MedicBook.TilQuizCode.model.TriviaQuestion.Topic.*

fun String.isValidEmail() : Boolean {
    return Patterns.EMAIL_ADDRESS.matcher(this).matches()
}

fun String.isValidPassword() : Boolean {
    return length >= 6
}

fun String.isValidNickname() : Boolean {
    return length >= 3
}

fun Int.random(): Int {
    return java.util.Random().nextInt(this)
}

fun View.shake() {
    (AnimatorInflater.loadAnimator(context, R.animator.shake) as AnimatorSet).apply {
        setTarget(this@shake)
        start()
    }
}

fun TriviaQuestion.Topic.Companion.correspondingBubbleIDForTopic(topic: TriviaQuestion.Topic): Int {
    return when (topic) {
        anatomy -> R.drawable.anatomy_bubble
        cpr -> R.drawable.cpr_bubble
        routine -> R.drawable.rutine_bubble
        medicine -> R.drawable.medicine_bubble
        teamWork-> R.drawable.medicine_bubble
        anamnesis -> R.drawable.anamnesis_bubble
        trauma -> R.drawable.trauma_bubble
        mentalHealth -> R.drawable.mental_health_bubble
        publicHealth -> R.drawable.public_health_bubble
        nbc -> R.drawable.nbc_bubble
        other -> R.drawable.nbc_bubble
    }
}
fun TriviaQuestion.Topic.Companion.titleResourceForTopic(topic: TriviaQuestion.Topic) : Int {
    return when (topic) {
        anatomy -> R.string.title_trivia_topic_anatomy
        cpr -> R.string.title_trivia_topic_cpr
        routine -> R.string.title_trivia_topic_routine
        medicine -> R.string.title_trivia_topic_medicine
        teamWork -> R.string.title_trivia_topic_teamWork
        anamnesis -> R.string.title_trivia_topic_anamnesis
        trauma -> R.string.title_trivia_topic_trauma
        mentalHealth -> R.string.title_trivia_topic_mentalHealth
        publicHealth -> R.string.title_trivia_topic_publicHealth
        nbc -> R.string.title_trivia_topic_nbc
        other -> R.string.title_trivia_topic_other
    }
}

