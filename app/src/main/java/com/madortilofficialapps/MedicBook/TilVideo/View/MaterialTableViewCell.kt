package com.madortilofficialapps.MedicBook.TilVideo.View

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.madortilofficialapps.MedicBook.R
import com.madortilofficialapps.MedicBook.TilVideo.Model.Material
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.material_item.*

class MaterialTableViewAdapter(private val cellMaterials : MutableList<Material>, private val searchString: String, private val onClick: (Int) -> Unit): RecyclerView.Adapter<MaterialTableViewAdapter.MaterialViewHolder>() {

    var cellMaterial : Material? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MaterialViewHolder {
        return MaterialViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.material_item, parent, false))
    }

    override fun getItemCount(): Int {
        return cellMaterials.size
    }

    override fun onBindViewHolder(holder: MaterialViewHolder, position: Int) {
        cellMaterial = cellMaterials[position]
        holder.bind(cellMaterial, searchString, position, onClick)
    }
    override fun onViewAttachedToWindow(holder: MaterialViewHolder) {
        super.onViewAttachedToWindow(holder)
    }
    class MaterialViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        var cellMaterial : Material? = null
        fun bind(material : Material?, searchString: String, position: Int, onClick: (Int) -> Unit){
            this.cellMaterial = material
            if(cellMaterial != null){
                materialName.text = cellMaterial!!.materialName
                if(searchString != ""){
                    if(cellMaterial!!.materialName.contains(searchString)){
                        this.itemView.visibility = View.VISIBLE
                    }else{
                        this.itemView.visibility = View.GONE
                        this.itemView.setLayoutParams(RecyclerView.LayoutParams(this.itemView.width, 0))
                    }
                }else{
                    this.itemView.visibility = View.VISIBLE
                }
            }
            containerView.setOnClickListener {
                onClick(position)
            }
        }
    }

}
