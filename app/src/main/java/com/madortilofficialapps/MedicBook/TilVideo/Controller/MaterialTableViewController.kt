package com.madortilofficialapps.MedicBook.TilVideo.Controller

import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.madortilofficialapps.MedicBook.TilVideo.Model.LoadingProtocol
import com.madortilofficialapps.MedicBook.TilVideo.Model.Material
import com.madortilofficialapps.MedicBook.TilVideo.Model.MenuBarDatabaseFacade
import com.madortilofficialapps.MedicBook.TilVideo.Model.refresher
import com.madortilofficialapps.MedicBook.TilVideo.View.MaterialTableViewAdapter
import kotlinx.android.synthetic.main.fragment_material_view.*


class MaterialTableViewController: Fragment(), refresher {

    var topic = MenuViewController.topicChosen
    private var Materials : MutableList<Material> = mutableListOf<Material>()
    var selectedMaterial : Material? = null
    var delegate : LoadingProtocol? = null
    var isRunning = true
    var searchString = ""


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(com.madortilofficialapps.MedicBook.R.layout.fragment_material_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        topic = MenuViewController.topicChosen
        prepareMaterialDatabase()
        materialRecyclerView.layoutManager = LinearLayoutManager(activity)
        materialRecyclerView.adapter = MaterialTableViewAdapter(Materials, searchString) { position ->
            openMaterial(position)
        }
        materialSearchBar.addTextChangedListener(object : TextWatcher{
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                searchString = s.toString()
                searchString = s.toString()
                reloadList()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
            }

        })
    }

    fun prepareMaterialDatabase(){
        MenuBarDatabaseFacade.databaseSingleton.materialDelegate = this
        Materials = Material.getMaterials(topic)
        if(Materials.count() == 0){
            //MenuBarDatabaseFacade.databaseSingleton.getMaterials(topic)
        }
    }

    fun reloadList(){
        materialRecyclerView.layoutManager = LinearLayoutManager(activity)
        materialRecyclerView.adapter = MaterialTableViewAdapter(Materials, searchString) { position ->
            openMaterial(position)
        }
    }

    fun reloadView(){
        if(isRunning) {
            val ft = fragmentManager!!.beginTransaction()
            if (Build.VERSION.SDK_INT >= 26) {
                ft.setReorderingAllowed(false)
            }
            ft.detach(this).attach(this).commit()
        }
    }
    override fun refreshUI() {
        Materials = Material.getMaterials(topic)
        if (delegate != null && MenuViewController.tabSelected == Tabs.Pdf){
            delegate?.finishLoading()
        }
        reloadView()
    }

    fun openMaterial(position : Int)
    {
        val parent = parentFragment as MenuViewController
        val url = Materials[position].getURL()
        selectedMaterial = Materials[position]
        if (Materials[position] != null){
            parent.openPdf(Materials[position])
        }
    }
    fun getMaterials() : MutableList<Material>{
        return this.Materials
    }

    //MARK Get functions



    override fun onPause() {
        super.onPause()
        isRunning = false
    }

    override fun onResume() {
        super.onResume()
        isRunning = true
    }

}
