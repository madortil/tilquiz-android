package com.madortilofficialapps.MedicBook.TilVideo.View

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.madortilofficialapps.MedicBook.R
import com.madortilofficialapps.MedicBook.TilQuizCode.model.TriviaQuestion
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.main_menu_item.*

class MainMenuTableViewAdapter(private val topicsCells: Array<TriviaQuestion.Topic>,
                               private val onClick: (Int) -> Unit)
    :RecyclerView.Adapter<MainMenuTableViewAdapter.MainMenuViewHolder>(){

    var topic: TriviaQuestion.Topic? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainMenuViewHolder {
        return MainMenuViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.main_menu_item, parent, false))
    }
    override fun getItemCount(): Int {
        return topicsCells.size
    }
    override fun onBindViewHolder(holder: MainMenuViewHolder, position: Int) {
        topic = topicsCells[position]
        holder.bind(topic, position, onClick)
    }
    override fun onViewAttachedToWindow(holder: MainMenuViewHolder) {
        super.onViewAttachedToWindow(holder)
    }
    class MainMenuViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        var topic: TriviaQuestion.Topic? = null
        fun bind(topic: TriviaQuestion.Topic?, position: Int, onClick: (Int) -> Unit) {
            this.topic = topic
            when(topic) {
                TriviaQuestion.Topic.trauma -> topicTabImage.setImageResource(R.drawable.trauma)
                TriviaQuestion.Topic.anamnesis -> topicTabImage.setImageResource(R.drawable.anamnesis)
                TriviaQuestion.Topic.anatomy -> topicTabImage.setImageResource(R.drawable.anatomy)
                TriviaQuestion.Topic.teamWork -> topicTabImage.setImageResource(R.drawable.team_work)
                TriviaQuestion.Topic.medicine -> topicTabImage.setImageResource(R.drawable.medicine)
                TriviaQuestion.Topic.routine -> topicTabImage.setImageResource(R.drawable.routine)
                TriviaQuestion.Topic.nbc -> topicTabImage.setImageResource(R.drawable.nbc)
                TriviaQuestion.Topic.cpr -> topicTabImage.setImageResource(R.drawable.cpr)
                TriviaQuestion.Topic.mentalHealth -> topicTabImage.setImageResource(R.drawable.mental_health)
                TriviaQuestion.Topic.publicHealth -> topicTabImage.setImageResource(R.drawable.public_health)
                TriviaQuestion.Topic.other -> topicTabImage.setImageResource(R.drawable.other) //Change to other button
            }
            containerView.setOnClickListener {
                onClick(position)
            }
        }
    }

}

