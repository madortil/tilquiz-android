package com.madortilofficialapps.MedicBook.TilVideo.Model
import com.madortilofficialapps.MedicBook.TilQuizCode.model.TriviaQuestion

data class Material(var materialURL: String, var materialName: String) {

    companion object {
        var traumaMaterials: MutableList<Material> = mutableListOf<Material>()
        var anamnesisMaterials: MutableList<Material> = mutableListOf<Material>()
        var anatomyMaterials: MutableList<Material> = mutableListOf<Material>()
        var cprMaterials: MutableList<Material> = mutableListOf<Material>()
        var nbcMaterials: MutableList<Material> = mutableListOf<Material>()
        var medicineMaterials: MutableList<Material> = mutableListOf<Material>()
        var teamWorkMaterials: MutableList<Material> = mutableListOf<Material>()
        var publicHealthMaterials: MutableList<Material> = mutableListOf<Material>()
        var mentalHealthMaterials: MutableList<Material> = mutableListOf<Material>()
        var routineMaterials: MutableList<Material> = mutableListOf<Material>()
        var otherMaterials: MutableList<Material> = mutableListOf<Material>()
        val medicine = "Medicine"
        val trauma = "Trauma"
        val anamnesis = "Anamnesis"
        val anatomy = "Anatomy"
        val routine = "Routine"
        val cpr = "Cpr"
        val nbc = "Nbc"
        val mentalHealth = "MentalHealth"
        val teamWork = "TeamWork"
        val publicHealth = "PublicHealth"
        val other = "Other"
        var isLoaded = false

        fun giveMaterial(videoTopic : String, material : Material){
            isLoaded = true
            when(videoTopic){
                medicine -> {
                    Material.medicineMaterials.add(material)
                }
                cpr -> {
                    Material.cprMaterials.add(material)
                }
                trauma -> {
                    Material.traumaMaterials.add(material)
                }
                anamnesis -> {
                    Material.anamnesisMaterials.add(material)
                }
                anatomy -> {
                    Material.anatomyMaterials.add(material)
                }
                mentalHealth -> {
                    Material.mentalHealthMaterials.add(material)
                }
                publicHealth -> {
                    Material.publicHealthMaterials.add(material)
                }
                teamWork -> {
                    Material.teamWorkMaterials.add(material)
                }
                nbc -> {
                    Material.nbcMaterials.add(material)
                }
                routine -> {
                    Material.routineMaterials.add(material)
                }
                other -> {
                    Material.otherMaterials.add(material)
                }
            }
        }

        fun getMaterials(topic: TriviaQuestion.Topic): MutableList<Material> {
            when(topic){
                TriviaQuestion.Topic.trauma ->{
                    return traumaMaterials
                }
                TriviaQuestion.Topic.anamnesis ->{
                    return anamnesisMaterials
                }
                TriviaQuestion.Topic.anatomy ->{
                    return anatomyMaterials
                }
                TriviaQuestion.Topic.mentalHealth ->{
                    return mentalHealthMaterials
                }
                TriviaQuestion.Topic.cpr ->{
                    return cprMaterials
                }
                TriviaQuestion.Topic.medicine->{
                    return medicineMaterials
                }
                TriviaQuestion.Topic.teamWork ->{
                    return teamWorkMaterials
                }
                TriviaQuestion.Topic.publicHealth ->{
                    return publicHealthMaterials
                }
                TriviaQuestion.Topic.nbc ->{
                    return nbcMaterials
                }
                TriviaQuestion.Topic.routine ->{
                    return routineMaterials
                }
                TriviaQuestion.Topic.other ->{
                    return otherMaterials
                }
            }
        }

        fun giveMaterials(topic: TriviaQuestion.Topic, materials: MutableList<Material>) {
            when (topic) {
                TriviaQuestion.Topic.trauma -> {
                    Material.traumaMaterials.clear()
                    for (material in materials) {
                        Material.traumaMaterials.add(material)
                    }
                }
                TriviaQuestion.Topic.routine -> {
                    Material.routineMaterials.clear()
                    for (material in materials) {
                        Material.routineMaterials.add(material)
                    }
                }
                TriviaQuestion.Topic.nbc -> {
                    Material.nbcMaterials.clear()
                    for (material in materials) {
                        Material.nbcMaterials.add(material)
                    }
                }
                TriviaQuestion.Topic.publicHealth -> {
                    Material.publicHealthMaterials.clear()
                    for (material in materials) {
                        Material.publicHealthMaterials.add(material)
                    }
                }
                TriviaQuestion.Topic.medicine -> {
                    Material.medicineMaterials.clear()
                    for (material in materials) {
                        Material.medicineMaterials.add(material)
                    }
                }
                TriviaQuestion.Topic.teamWork -> {
                    Material.teamWorkMaterials.clear()
                    for (material in materials) {
                        Material.teamWorkMaterials.add(material)
                    }
                }
                TriviaQuestion.Topic.cpr -> {
                    Material.cprMaterials.clear()
                    for (material in materials) {
                        Material.cprMaterials.add(material)
                    }
                }
                TriviaQuestion.Topic.mentalHealth -> {
                    Material.mentalHealthMaterials.clear()
                    for (material in materials) {
                        Material.mentalHealthMaterials.add(material)
                    }
                }
                TriviaQuestion.Topic.anatomy -> {
                    Material.anatomyMaterials.clear()
                    for (material in materials) {
                        Material.anatomyMaterials.add(material)
                    }
                }
                TriviaQuestion.Topic.anamnesis -> {
                    Material.anamnesisMaterials.clear()
                    for (material in materials) {
                        Material.anamnesisMaterials.add(material)
                    }
                }
                TriviaQuestion.Topic.other -> {
                    Material.otherMaterials.clear()
                    for (material in materials) {
                        Material.otherMaterials.add(material)
                    }
                }
            }
        }
    }

    fun getName() : String{
        return materialName
    }
    fun getURL() : String{
        return materialURL
    }
}
