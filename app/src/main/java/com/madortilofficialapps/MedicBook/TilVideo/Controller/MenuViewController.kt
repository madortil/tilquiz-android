package com.madortilofficialapps.MedicBook.TilVideo.Controller
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.navigation.fragment.findNavController
import com.madortilofficialapps.MedicBook.R
import com.madortilofficialapps.MedicBook.TilQuizCode.model.TriviaQuestion
import com.madortilofficialapps.MedicBook.TilQuizCode.view_controllers.TestViewController
import com.madortilofficialapps.MedicBook.TilVideo.Model.LoadingProtocol
import com.madortilofficialapps.MedicBook.TilVideo.Model.Material
import com.madortilofficialapps.MedicBook.TilVideo.Model.Video
import kotlinx.android.synthetic.main.fragment_menu_view.*

class MenuViewController: Fragment(), LoadingProtocol {

    private var materialViewController = MaterialTableViewController()
    private var videoTableViewController = VideoTableViewController()
    private var triviaViewController = TestViewController()
    private var done = false
    companion object{
        var tabSelected = Tabs.Pdf
        var topicChosen = TriviaQuestion.Topic.trauma
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(com.madortilofficialapps.MedicBook.R.layout.fragment_menu_view, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        beginLoadingMaterials()
        if(!done){
            childFragmentManager.beginTransaction().apply {
                add(R.id.fragmentFrame, videoTableViewController, "videoView")
                add(R.id.fragmentFrame, materialViewController, "materialView")
                add(R.id.fragmentFrame, triviaViewController, "triviaView")
                hide(videoTableViewController)
                hide(triviaViewController)
                show(materialViewController)
                setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                commit()
            }
            childFragmentManager.executePendingTransactions()
        }

        done = true
        triviaViewController = childFragmentManager.findFragmentByTag("triviaView") as TestViewController
        videoTableViewController = childFragmentManager.findFragmentByTag("videoView") as VideoTableViewController
        materialViewController = childFragmentManager.findFragmentByTag("materialView") as MaterialTableViewController
        videoTableViewController.delegate = this
        materialViewController.delegate = this
        tabHandler()
        videoTabButton.setOnClickListener{ view->
            childFragmentManager.beginTransaction().apply {
                hide(materialViewController)
                hide(triviaViewController)
                show(videoTableViewController)
                setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                commit()
            }
            tabSelected = Tabs.Video
            tabHandler()
        }
        if(MenuViewController.topicChosen == TriviaQuestion.Topic.other){
            triviaTabButton.visibility = View.GONE
        }else{
            triviaTabButton.visibility = View.VISIBLE
        }
        triviaTabButton.setOnClickListener { view->
            childFragmentManager.beginTransaction().apply {
                hide(materialViewController)
                hide(videoTableViewController)
                show(triviaViewController)
                setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                commit()
            }
            childFragmentManager.beginTransaction().hide(videoTableViewController)
            childFragmentManager.beginTransaction().show(materialViewController)
            tabSelected = Tabs.Quiz
            tabHandler()

        }
        PDFTabButton.setOnClickListener { view->
            childFragmentManager.beginTransaction().apply {
                show(materialViewController)
                hide(videoTableViewController)
                hide(triviaViewController)
                setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                commit()
            }
            childFragmentManager.beginTransaction().hide(videoTableViewController)
            childFragmentManager.beginTransaction().show(materialViewController)
            tabSelected = Tabs.Pdf
            tabHandler()

        }
    }

    fun tabHandler(){
        when(MenuViewController.tabSelected){
            Tabs.Quiz -> {
                triviaTabButton.setColorFilter(ContextCompat.getColor(context!!, com.madortilofficialapps.MedicBook.R.color.darkGray))
                videoTabButton.setColorFilter(ContextCompat.getColor(context!!, com.madortilofficialapps.MedicBook.R.color.lightGray))
                PDFTabButton.setColorFilter(ContextCompat.getColor(context!!, com.madortilofficialapps.MedicBook.R.color.lightGray))
                finishLoading()
                pdfUnderline.visibility = View.GONE
                triviaUnderline.visibility = View.VISIBLE
                videoUnderline.visibility = View.GONE
            }
            Tabs.Video -> {
                triviaTabButton.setColorFilter(ContextCompat.getColor(context!!, com.madortilofficialapps.MedicBook.R.color.lightGray))
                videoTabButton.setColorFilter(ContextCompat.getColor(context!!, com.madortilofficialapps.MedicBook.R.color.darkGray))
                PDFTabButton.setColorFilter(ContextCompat.getColor(context!!, com.madortilofficialapps.MedicBook.R.color.lightGray))
                pdfUnderline.visibility = View.GONE
                triviaUnderline.visibility = View.GONE
                videoUnderline.visibility = View.VISIBLE
                if (!Video.isLoaded) {
                  startLoading()
                }else{
                    finishLoading()
                }

            }
            Tabs.Pdf -> {
                triviaTabButton.setColorFilter(ContextCompat.getColor(context!!, com.madortilofficialapps.MedicBook.R.color.lightGray))
                videoTabButton.setColorFilter(ContextCompat.getColor(context!!, com.madortilofficialapps.MedicBook.R.color.lightGray))
                PDFTabButton.setColorFilter(ContextCompat.getColor(context!!, com.madortilofficialapps.MedicBook.R.color.darkGray))
                pdfUnderline.visibility = View.VISIBLE
                triviaUnderline.visibility = View.GONE
                videoUnderline.visibility = View.GONE
                beginLoadingMaterials()
                when(topicChosen){
                    TriviaQuestion.Topic.trauma -> {
                        if(materialViewController.getMaterials() != Material.traumaMaterials){
                            materialViewController.reloadView()
                        }
                    }
                    TriviaQuestion.Topic.anamnesis -> {
                        if(materialViewController.getMaterials() != Material.anamnesisMaterials){
                            materialViewController.reloadView()
                        }
                    }
                    TriviaQuestion.Topic.anatomy -> {
                        if(materialViewController.getMaterials() != Material.anatomyMaterials){
                            materialViewController.reloadView()
                        }
                    }
                    TriviaQuestion.Topic.cpr -> {
                        if(materialViewController.getMaterials() != Material.cprMaterials){
                            materialViewController.reloadView()
                        }
                    }
                    TriviaQuestion.Topic.medicine -> {
                        if(materialViewController.getMaterials() != Material.medicineMaterials){
                            materialViewController.reloadView()
                        }
                    }
                    TriviaQuestion.Topic.mentalHealth -> {
                        if(materialViewController.getMaterials() != Material.mentalHealthMaterials){
                            materialViewController.reloadView()
                        }
                    }
                    TriviaQuestion.Topic.nbc -> {
                        if(materialViewController.getMaterials() != Material.nbcMaterials){
                            materialViewController.reloadView()
                        }
                    }
                    TriviaQuestion.Topic.other -> {
                        if(materialViewController.getMaterials() != Material.otherMaterials){
                            materialViewController.reloadView()
                        }
                    }
                    TriviaQuestion.Topic.publicHealth -> {
                        if(materialViewController.getMaterials() != Material.publicHealthMaterials){
                            materialViewController.reloadView()
                        }
                    }
                    TriviaQuestion.Topic.routine -> {
                        if(materialViewController.getMaterials() != Material.routineMaterials){
                            materialViewController.reloadView()
                        }
                    }
                    TriviaQuestion.Topic.teamWork -> {
                        if(materialViewController.getMaterials() != Material.teamWorkMaterials){
                            materialViewController.reloadView()
                        }
                    }
                }
            }
        }
    }
    public fun startTrivia(){
        findNavController().navigate(R.id.action_MenuViewFragment_to_triviaGameFragment)
    }
    public fun openPdf(selectedMaterial : Material){
        var materialUrlBundle = bundleOf("url" to selectedMaterial.getURL())
        findNavController().navigate(R.id.action_MenuViewFragment_to_webViewPdf, materialUrlBundle)
    }
    public fun openVideo(selectedVideo: Video){
        var videoUrlBundle = bundleOf("url" to selectedVideo.videoURL)
        findNavController().navigate(R.id.action_MenuViewFragment_to_videoViewController, videoUrlBundle)
    }

    //MARK - LoadingProtocol Functions

    override fun finishLoading() {
        if(menuLoadingPanel != null){
            menuLoadingPanel.visibility = View.GONE
        }
    }

    override fun startLoading() {
        if(menuLoadingPanel != null){
            menuLoadingPanel.visibility = View.VISIBLE
        }
    }

    override fun onPause() {
        super.onPause()
        //MenuBarDatabaseFacade.databaseSingleton.stopLoading()
    }

    override fun onResume() {
        super.onResume()
    }
    fun beginLoadingMaterials(){
        if(MenuViewController.tabSelected == Tabs.Pdf && !Material.isLoaded || MenuViewController.tabSelected == Tabs.Video && !Video.isLoaded){
            startLoading()
        }else{
            finishLoading()
        }
    }

}

enum class Tabs {
    Video,
    Quiz,
    Pdf

}
