package com.madortilofficialapps.MedicBook.TilVideo.Model

interface LoadingProtocol{
    fun finishLoading()
    fun startLoading()
}
