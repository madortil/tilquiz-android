package com.madortilofficialapps.MedicBook.TilVideo.Controller

import android.content.pm.ActivityInfo
import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_pdf_view.*

class WebViewPdf : Fragment() {

    lateinit var webView: WebView
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (pdfLoadingPanel != null) {
            pdfLoadingPanel.visibility = View.GONE
        }
        var view = inflater.inflate(com.madortilofficialapps.MedicBook.R.layout.fragment_pdf_view, container, false)
        webView = view.findViewById(
                com.madortilofficialapps.MedicBook.R.id.webView) as WebView
        var googleDocs = "https://docs.google.com/viewer?embedded=true&url="
        var url = arguments!!.getString("url")
        url = googleDocs + url
        webView.invalidate()
        webView.settings.javaScriptEnabled = true
        webView.settings.setSupportZoom(true)
        webView.settings.setBuiltInZoomControls(true)
        webView.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null)
        webView.settings.domStorageEnabled = true
        var didLoadPDF = false
        webView.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                if (pdfLoadingPanel != null) {
                    pdfLoadingPanel.visibility = View.VISIBLE
                }
                didLoadPDF = true
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
               if(!didLoadPDF){
                    webView.loadUrl(url)
                }else {
                    if (pdfLoadingPanel != null) {
                        pdfLoadingPanel.visibility = View.GONE
                    }
                   if(webView.title == ""){
                       webView.reload()
                   }
                }
            }
        }
        webView.loadUrl(url)
        return view
    }

    override fun onPause() {
        super.onPause()
        webView.clearCache(true)
        webView.getSettings().setAppCacheEnabled(false);
        activity?.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        webView.destroy()
    }

    override fun onResume() {
        super.onResume()
        activity?.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR)
    }
}
