package com.madortilofficialapps.MedicBook.TilVideo.Model

//
//  tilPortal
//
//  Created by Mador Til on 07/10/2019.
//  Copyright © 2019 Yahav Levi. All rights reserved.
//
import com.madortilofficialapps.MedicBook.TilQuizCode.model.TriviaQuestion

data class Video(var videoURL : String, var videoThumbnailURL : String, var videoName : String, var Topic : TriviaQuestion.Topic) {
    companion object{
        var isLoaded = false
        val medicine = "Medicine"
        val trauma = "Trauma"
        val anamnesis = "Anamnesis"
        val anatomy = "Anatomy"
        val routine = "Routine"
        val cpr = "Cpr"
        val nbc = "Nbc"
        val mentalHealth = "MentalHealth"
        val teamWork = "TeamWork"
        val publicHealth = "PublicHealth"
        val other = "Other"
        var traumaVideos : MutableList<Video> = mutableListOf<Video>()
        var cprVideos : MutableList<Video> = mutableListOf<Video>()
        var medicineVideos : MutableList<Video> = mutableListOf<Video>()
        var teamWorkVideos : MutableList<Video> = mutableListOf<Video>()
        var publicHealthVideos : MutableList<Video> = mutableListOf<Video>()
        var mentalHealthVideos : MutableList<Video> = mutableListOf<Video>()
        var anamnesisVideos : MutableList<Video> = mutableListOf<Video>()
        var anatomyVideos : MutableList<Video> = mutableListOf<Video>()
        var nbcVideos : MutableList<Video> = mutableListOf<Video>()
        var routineVideos : MutableList<Video> = mutableListOf<Video>()
        var otherVideos : MutableList<Video> = mutableListOf<Video>()
        fun getVideos(topic : TriviaQuestion.Topic) : MutableList<Video>{
            when(topic){
                TriviaQuestion.Topic.trauma ->{
                    return traumaVideos
                }
                TriviaQuestion.Topic.anamnesis->{
                    return anamnesisVideos
                }
                TriviaQuestion.Topic.anatomy ->{
                    return anatomyVideos
                }
                TriviaQuestion.Topic.mentalHealth ->{
                    return mentalHealthVideos
                }
                TriviaQuestion.Topic.cpr ->{
                    return cprVideos
                }
                TriviaQuestion.Topic.medicine ->{
                    return medicineVideos
                }
                TriviaQuestion.Topic.teamWork ->{
                    return teamWorkVideos
                }
                TriviaQuestion.Topic.publicHealth ->{
                    return publicHealthVideos
                }
                TriviaQuestion.Topic.nbc ->{
                    return nbcVideos
                }
                TriviaQuestion.Topic.routine ->{
                    return routineVideos
                }
                TriviaQuestion.Topic.other ->{
                    return otherVideos
                }
            }
        }
        fun giveVideo(videoTopic : String, video : Video){
            isLoaded = true
            when(videoTopic){
                medicine -> {
                    Video.medicineVideos.add(video)
                }
                cpr -> {
                    Video.cprVideos.add(video)
                }
                trauma -> {
                    Video.traumaVideos.add(video)
                }
                anamnesis -> {
                    Video.anamnesisVideos.add(video)
                }
                anatomy -> {
                    Video.anatomyVideos.add(video)
                }
                mentalHealth -> {
                    Video.mentalHealthVideos.add(video)
                }
                publicHealth -> {
                    Video.publicHealthVideos.add(video)
                }
                teamWork -> {
                    Video.teamWorkVideos.add(video)
                }
                nbc -> {
                    Video.nbcVideos.add(video)
                }
                routine -> {
                    Video.routineVideos.add(video)
                }
                other -> {
                    Video.otherVideos.add(video)
                }
            }
        }
        fun giveVideos(topic : TriviaQuestion.Topic, videos : MutableList<Video>){
            isLoaded = true
            when(topic){
                TriviaQuestion.Topic.trauma -> {
                    Video.traumaVideos.clear()
                    for(video in videos){
                        Video.traumaVideos.add(video)
                    }
                }
                TriviaQuestion.Topic.routine -> {
                    Video.routineVideos.clear()
                    for(video in videos){
                        Video.routineVideos.add(video)
                    }
                }
                TriviaQuestion.Topic.nbc -> {
                    Video.nbcVideos.clear()
                    for(video in videos){
                        Video.nbcVideos.add(video)
                    }
                }
                TriviaQuestion.Topic.publicHealth -> {
                    Video.publicHealthVideos.clear()
                    for(video in videos){
                        Video.publicHealthVideos.add(video)
                    }
                }
                TriviaQuestion.Topic.medicine -> {
                    Video.medicineVideos.clear()
                    for(video in videos){
                        Video.medicineVideos.add(video)
                    }
                }
                TriviaQuestion.Topic.teamWork -> {
                    Video.teamWorkVideos.clear()
                    for(video in videos){
                        Video.teamWorkVideos.add(video)
                    }
                }
                TriviaQuestion.Topic.cpr -> {
                    Video.cprVideos.clear()
                    for(video in videos){
                        Video.cprVideos.add(video)
                    }
                }
                TriviaQuestion.Topic.mentalHealth -> {
                    Video.mentalHealthVideos.clear()
                    for(video in videos){
                        Video.mentalHealthVideos.add(video)
                    }
                }
                TriviaQuestion.Topic.anatomy -> {
                    Video.anatomyVideos.clear()
                    for(video in videos){
                        Video.anatomyVideos.add(video)
                    }
                }
                TriviaQuestion.Topic.anamnesis -> {
                    Video.anamnesisVideos.clear()
                    for(video in videos){
                        Video.anamnesisVideos.add(video)
                    }
                }
                TriviaQuestion.Topic.other -> {
                    Video.otherVideos.clear()
                    for(video in videos){
                        Video.otherVideos.add(video)
                    }
                }
            }
        }
    }
    fun getName() : String{
        return videoName
    }
    fun getThumbnail() : String{
        return videoThumbnailURL
    }
    fun getURL() : String{
        return videoURL
    }
}
