package com.madortilofficialapps.MedicBook.TilVideo.Model

import android.graphics.drawable.Drawable

class VideoThumbnailImages(){
    companion object{
        var traumaThumbnails : MutableList<Drawable> = mutableListOf<Drawable>()
        var traumaThumbnailVideos : MutableList<Video> = mutableListOf<Video>()
        var anatomyThumbnails : MutableList<Drawable> = mutableListOf<Drawable>()
        var anatomyThumbnailVideos : MutableList<Video> = mutableListOf<Video>()
        var anamnesisThumbnails : MutableList<Drawable> = mutableListOf<Drawable>()
        var anamnesisThumbnailVideos : MutableList<Video> = mutableListOf<Video>()
        var mentalHealthThumbnails : MutableList<Drawable> = mutableListOf<Drawable>()
        var mentalHealthThumbnailVideos : MutableList<Video> = mutableListOf<Video>()
        var publicHealthThumbnails : MutableList<Drawable> = mutableListOf<Drawable>()
        var publicHealthThumbnailVideos : MutableList<Video> = mutableListOf<Video>()
        var medicineThumbnails : MutableList<Drawable> = mutableListOf<Drawable>()
        var medicineThumbnailVideos : MutableList<Video> = mutableListOf<Video>()
        var cprThumbnails : MutableList<Drawable> = mutableListOf<Drawable>()
        var cprThumbnailVideos : MutableList<Video> = mutableListOf<Video>()
        var teamWorkThumbnails : MutableList<Drawable> = mutableListOf<Drawable>()
        var teamWorkThumbnailVideos : MutableList<Video> = mutableListOf<Video>()
        var nbcThumbnails : MutableList<Drawable> = mutableListOf<Drawable>()
        var nbcThumbnailVideos : MutableList<Video> = mutableListOf<Video>()
        var routineThumbnails : MutableList<Drawable> = mutableListOf<Drawable>()
        var routineThumbnailVideos : MutableList<Video> = mutableListOf<Video>()
        var otherThumbnails : MutableList<Drawable> = mutableListOf<Drawable>()
        var otherThumbnailVideos : MutableList<Video> = mutableListOf<Video>()

    }
}
