package com.madortilofficialapps.MedicBook.TilVideo.Model
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.madortilofficialapps.MedicBook.TilQuizCode.model.TriviaQuestion

class MenuBarDatabaseFacade{

    lateinit var ref : DatabaseReference
    var videoDelegate: refresher? = null
    val medicine = "Medicine"
    val trauma = "Trauma"
    val anamnesis = "Anamnesis"
    val anatomy = "Anatomy"
    val routine = "Routine"
    val cpr = "Cpr"
    val nbc = "Nbc"
    val mentalHealth = "MentalHealth"
    val teamWork = "TeamWork"
    val publicHealth = "PublicHealth"
    val other = "Other"
    var materialDelegate: refresher? = null
    var didNotLoad = true
    lateinit var videoQuery : Query
    lateinit var materialQuery: Query
    lateinit var materialValueEventListener : ValueEventListener
    lateinit var videoValueEventListener : ValueEventListener
    companion object{
        public val databaseSingleton = MenuBarDatabaseFacade()
    }
    private constructor(){
        //init reference
        ref = FirebaseDatabase.getInstance().getReference("Videos")
    }
    public fun getVideosAndMaterials(){
        if(didNotLoad){
            didNotLoad = false
            videoQuery = FirebaseDatabase.getInstance().getReference("Videos")
            videoQuery.addListenerForSingleValueEvent(object : ValueEventListener{
                override fun onCancelled(p0: DatabaseError) {
                }

                override fun onDataChange(snapshots: DataSnapshot) {
                    if(snapshots.exists()){
                        for(videos in snapshots.children){
                            val videoObject = videos.value as? Map<String, Any?>
                            val videoTopic = videoObject!!["Topic"] as String
                            var video : Video
                            video = Video(videoObject!!["VideoURL"] as String, videoObject!!["ThumbnailURL"] as String, videoObject!!["Name"] as String, TriviaQuestion.Topic.anamnesis)
                            when(videoTopic){
                                medicine ->{
                                    video = Video(videoObject!!["VideoURL"] as String, videoObject!!["ThumbnailURL"] as String, videoObject!!["Name"] as String, TriviaQuestion.Topic.medicine)
                                }
                                anamnesis ->{
                                    video = Video(videoObject!!["VideoURL"] as String, videoObject!!["ThumbnailURL"] as String, videoObject!!["Name"] as String, TriviaQuestion.Topic.anamnesis)
                                }
                                trauma ->{
                                    video = Video(videoObject!!["VideoURL"] as String, videoObject!!["ThumbnailURL"] as String, videoObject!!["Name"] as String, TriviaQuestion.Topic.trauma)
                                }
                                anatomy ->{
                                    video = Video(videoObject!!["VideoURL"] as String, videoObject!!["ThumbnailURL"] as String, videoObject!!["Name"] as String, TriviaQuestion.Topic.anatomy)
                                }
                                routine ->{
                                    video = Video(videoObject!!["VideoURL"] as String, videoObject!!["ThumbnailURL"] as String, videoObject!!["Name"] as String, TriviaQuestion.Topic.routine)
                                }
                                cpr ->{
                                    video = Video(videoObject!!["VideoURL"] as String, videoObject!!["ThumbnailURL"] as String, videoObject!!["Name"] as String, TriviaQuestion.Topic.cpr)
                                }
                                nbc ->{
                                    video = Video(videoObject!!["VideoURL"] as String, videoObject!!["ThumbnailURL"] as String, videoObject!!["Name"] as String, TriviaQuestion.Topic.nbc)
                                }
                                mentalHealth ->{
                                    video = Video(videoObject!!["VideoURL"] as String, videoObject!!["ThumbnailURL"] as String, videoObject!!["Name"] as String, TriviaQuestion.Topic.mentalHealth)
                                }
                                publicHealth ->{
                                    video = Video(videoObject!!["VideoURL"] as String, videoObject!!["ThumbnailURL"] as String, videoObject!!["Name"] as String, TriviaQuestion.Topic.publicHealth)
                                }
                                teamWork->{
                                    video = Video(videoObject!!["VideoURL"] as String, videoObject!!["ThumbnailURL"] as String, videoObject!!["Name"] as String, TriviaQuestion.Topic.teamWork)
                                }
                                other ->{
                                    video = Video(videoObject!!["VideoURL"] as String, videoObject!!["ThumbnailURL"] as String, videoObject!!["Name"] as String, TriviaQuestion.Topic.other)
                                }
                            }

                            Video.giveVideo(videoTopic, video)
                            if(videoDelegate != null){
                                videoDelegate?.refreshUI()
                            }
                        }
                    }
                }

            })
            materialQuery = FirebaseDatabase.getInstance().getReference("Materials")
            materialQuery.addListenerForSingleValueEvent(object : ValueEventListener{
                override fun onCancelled(p0: DatabaseError) {
                }

                override fun onDataChange(snapshots: DataSnapshot) {
                    if(snapshots.exists()){
                        for(materials in snapshots.children){
                            val materialObject = materials.value as? Map<String, Any?>
                            val materialTopic = materialObject!!["Topic"] as String
                            var url = materialObject!!["MaterialURL"] as String
                            FirebaseStorage.getInstance().reference.child(url).downloadUrl.addOnSuccessListener(OnSuccessListener {
                                var downloadURLLink = it.toString()
                                val material = Material(downloadURLLink, materialObject!!["Name"] as String)
                                Material.giveMaterial(materialTopic, material)
                                if(materialDelegate != null){
                                    materialDelegate?.refreshUI()
                                }
                            })
                        }
                    }
                }
            })
        }
    }
/*
    public fun getVideos(topic: TriviaQuestion.Topic){
        ref = FirebaseDatabase.getInstance().getReference("Videos")
        videoQuery = ref
        videoValueEventListener = videoQuery.addValueEventListener(object : ValueEventListener{
            var videoInfo : MutableList<Video> = mutableListOf<Video>()
            var currentTopic = ""
            override fun onDataChange(snapshots: DataSnapshot) {
                if(snapshots.exists()){
                    for(videos in snapshots.children){
                        val videoObject = videos.value as? Map<String, Any?>
                        currentTopic = videoObject!!["Topic"] as String
                        when(topic) {
                            TriviaQuestion.Topic.trauma -> {
                                if (currentTopic == trauma) {
                                    videoInfo.add(Video(videoObject!!["VideoURL"] as String, videoObject!!["ThumbnailURL"] as String, videoObject!!["Name"] as String, topic))
                                    Video.giveVideos(topic, videoInfo)
                                }
                            }
                            TriviaQuestion.Topic.anamnesis -> {
                                if (currentTopic == anamnesis) {
                                    videoInfo.add(Video(videoObject!!["VideoURL"] as String, videoObject!!["ThumbnailURL"] as String, videoObject!!["Name"] as String, topic))
                                    Video.giveVideos(topic, videoInfo)
                                }
                            }
                            TriviaQuestion.Topic.anatomy -> {
                                if (currentTopic == anatomy) {
                                    videoInfo.add(Video(videoObject!!["VideoURL"] as String, videoObject!!["ThumbnailURL"] as String, videoObject!!["Name"] as String, topic))
                                    Video.giveVideos(topic, videoInfo)
                                }
                            }
                            TriviaQuestion.Topic.cpr -> {
                                if (currentTopic == cpr) {
                                    videoInfo.add(Video(videoObject!!["VideoURL"] as String, videoObject!!["ThumbnailURL"] as String, videoObject!!["Name"] as String, topic))
                                    Video.giveVideos(topic, videoInfo)
                                }
                            }
                            TriviaQuestion.Topic.medicine -> {
                                if (currentTopic == medicine) {
                                    videoInfo.add(Video(videoObject!!["VideoURL"] as String, videoObject!!["ThumbnailURL"] as String, videoObject!!["Name"] as String, topic))
                                    Video.giveVideos(topic, videoInfo)
                                }
                            }
                            TriviaQuestion.Topic.teamWork -> {
                                if (currentTopic == teamWork) {
                                    videoInfo.add(Video(videoObject!!["VideoURL"] as String, videoObject!!["ThumbnailURL"] as String, videoObject!!["Name"] as String, topic))
                                    Video.giveVideos(topic, videoInfo)
                                }
                            }
                            TriviaQuestion.Topic.mentalHealth -> {
                                if (currentTopic == mentalHealth) {
                                    videoInfo.add(Video(videoObject!!["VideoURL"] as String, videoObject!!["ThumbnailURL"] as String, videoObject!!["Name"] as String, topic))
                                    Video.giveVideos(topic, videoInfo)
                                }
                            }
                            TriviaQuestion.Topic.nbc -> {
                                if (currentTopic == nbc) {
                                    videoInfo.add(Video(videoObject!!["VideoURL"] as String, videoObject!!["ThumbnailURL"] as String, videoObject!!["Name"] as String, topic))
                                    Video.giveVideos(topic, videoInfo)
                                }
                            }
                            TriviaQuestion.Topic.publicHealth -> {
                                if (currentTopic == publicHealth) {
                                    videoInfo.add(Video(videoObject!!["VideoURL"] as String, videoObject!!["ThumbnailURL"] as String, videoObject!!["Name"] as String, topic))
                                    Video.giveVideos(topic, videoInfo)
                                }
                            }
                            TriviaQuestion.Topic.routine -> {
                                if (currentTopic == routine) {
                                    videoInfo.add(Video(videoObject!!["VideoURL"] as String, videoObject!!["ThumbnailURL"] as String, videoObject!!["Name"] as String, topic))
                                    Video.giveVideos(topic, videoInfo)
                                }
                            }
                            TriviaQuestion.Topic.other -> {
                                if (currentTopic == other) {
                                    videoInfo.add(Video(videoObject!!["VideoURL"] as String, videoObject!!["ThumbnailURL"] as String, videoObject!!["Name"] as String, topic))
                                    Video.giveVideos(topic, videoInfo)
                                }
                            }
                        }
                        videoDelegate?.refreshUI()
                    }
                }
            }
            override fun onCancelled(snapshots: DatabaseError) {
            }
        })
    }

    public fun getMaterials(topic: TriviaQuestion.Topic){
        ref = FirebaseDatabase.getInstance().getReference("Materials")
        materialQuery = ref
        materialValueEventListener = materialQuery.addValueEventListener(object : ValueEventListener{
            var materialInfo : MutableList<Material> = mutableListOf<Material>()
            var currentTopic = ""
            override fun onDataChange(snapshots: DataSnapshot) {
                if(snapshots.exists()){
                    for(videos in snapshots.children){
                        val videoObject = videos.value as? Map<String, Any?>
                        currentTopic = videoObject!!["Topic"] as String
                        when(topic){
                            TriviaQuestion.Topic.trauma -> {
                                if(currentTopic == trauma){
                                    Log.d("Trauma snapshot", videoObject!!["MaterialURL"] as String)
                                    var url = videoObject!!["MaterialURL"] as String
                                    FirebaseStorage.getInstance().reference.child(url).downloadUrl.addOnSuccessListener(OnSuccessListener {
                                        var downloadURLLink = it.toString()
                                        materialInfo.add(Material(downloadURLLink, videoObject!!["Name"] as String))
                                        Material.giveMaterials(topic, materialInfo)
                                        materialDelegate?.refreshUI()
                                    })
                                }
                            }
                            TriviaQuestion.Topic.anamnesis -> {
                                if(currentTopic == anamnesis){
                                    var url = videoObject!!["MaterialURL"] as String
                                    FirebaseStorage.getInstance().reference.child(url).downloadUrl.addOnSuccessListener(OnSuccessListener {
                                        var downloadURLLink = it.toString()
                                        materialInfo.add(Material(downloadURLLink, videoObject!!["Name"] as String))
                                        Material.giveMaterials(topic, materialInfo)
                                        materialDelegate?.refreshUI()
                                    })
                                }
                            }
                            TriviaQuestion.Topic.anatomy -> {
                                if(currentTopic == anatomy){
                                    var url = videoObject!!["MaterialURL"] as String
                                    FirebaseStorage.getInstance().reference.child(url).downloadUrl.addOnSuccessListener(OnSuccessListener {
                                       var downloadURLLink = it.toString()
                                        materialInfo.add(Material(downloadURLLink, videoObject!!["Name"] as String))
                                        Material.giveMaterials(topic, materialInfo)
                                        materialDelegate?.refreshUI()
                                    })
                                }
                            }
                            TriviaQuestion.Topic.cpr -> {
                                if(currentTopic == cpr){
                                    var url = videoObject!!["MaterialURL"] as String
                                    FirebaseStorage.getInstance().reference.child(url).downloadUrl.addOnSuccessListener(OnSuccessListener {
                                        var downloadURLLink = it.toString()
                                        materialInfo.add(Material(downloadURLLink, videoObject!!["Name"] as String))
                                        Material.giveMaterials(topic, materialInfo)
                                        materialDelegate?.refreshUI()
                                    })
                                }
                            }
                            TriviaQuestion.Topic.medicine -> {
                                if(currentTopic == medicine){
                                    var url = videoObject!!["MaterialURL"] as String
                                    FirebaseStorage.getInstance().reference.child(url).downloadUrl.addOnSuccessListener(OnSuccessListener {
                                        var downloadURLLink = it.toString()
                                        materialInfo.add(Material(downloadURLLink, videoObject!!["Name"] as String))
                                        Material.giveMaterials(topic, materialInfo)
                                        materialDelegate?.refreshUI()
                                    })
                                }
                            }
                            TriviaQuestion.Topic.teamWork -> {
                                if(currentTopic == teamWork){
                                    var url = videoObject!!["MaterialURL"] as String
                                    FirebaseStorage.getInstance().reference.child(url).downloadUrl.addOnSuccessListener(OnSuccessListener {
                                        var downloadURLLink = it.toString()
                                        materialInfo.add(Material(downloadURLLink, videoObject!!["Name"] as String))
                                        Material.giveMaterials(topic, materialInfo)
                                        materialDelegate?.refreshUI()
                                    })
                                }
                            }
                            TriviaQuestion.Topic.mentalHealth -> {
                                if(currentTopic == mentalHealth){
                                    var url = videoObject!!["MaterialURL"] as String
                                    FirebaseStorage.getInstance().reference.child(url).downloadUrl.addOnSuccessListener(OnSuccessListener {
                                        var downloadURLLink = it.toString()
                                        materialInfo.add(Material(downloadURLLink, videoObject!!["Name"] as String))
                                        Material.giveMaterials(topic, materialInfo)
                                        materialDelegate?.refreshUI()
                                    })
                                }
                            }
                            TriviaQuestion.Topic.nbc -> {
                                if (currentTopic == nbc) {
                                    var url = videoObject!!["MaterialURL"] as String
                                    FirebaseStorage.getInstance().reference.child(url).downloadUrl.addOnSuccessListener(OnSuccessListener {
                                        var downloadURLLink = it.toString()
                                        materialInfo.add(Material(downloadURLLink, videoObject!!["Name"] as String))
                                        Material.giveMaterials(topic, materialInfo)
                                        materialDelegate?.refreshUI()
                                    })
                                }
                            }
                            TriviaQuestion.Topic.publicHealth -> {
                                if(currentTopic == publicHealth){
                                    var url = videoObject!!["MaterialURL"] as String
                                    FirebaseStorage.getInstance().reference.child(url).downloadUrl.addOnSuccessListener(OnSuccessListener {
                                        var downloadURLLink = it.toString()
                                        materialInfo.add(Material(downloadURLLink, videoObject!!["Name"] as String))
                                        Material.giveMaterials(topic, materialInfo)
                                        materialDelegate?.refreshUI()
                                    })
                                }
                            }
                            TriviaQuestion.Topic.routine -> {
                                if(currentTopic == routine){
                                    var url = videoObject!!["MaterialURL"] as String
                                    FirebaseStorage.getInstance().reference.child(url).downloadUrl.addOnSuccessListener(OnSuccessListener {
                                        var downloadURLLink = it.toString()
                                        materialInfo.add(Material(downloadURLLink, videoObject!!["Name"] as String))
                                        Material.giveMaterials(topic, materialInfo)
                                        materialDelegate?.refreshUI()
                                    })
                                }
                            }
                            TriviaQuestion.Topic.other -> {
                                if(currentTopic == other){
                                    var url = videoObject!!["MaterialURL"] as String
                                    FirebaseStorage.getInstance().reference.child(url).downloadUrl.addOnSuccessListener(OnSuccessListener {
                                        var downloadURLLink = it.toString()
                                        materialInfo.add(Material(downloadURLLink, videoObject!!["Name"] as String))
                                        Material.giveMaterials(topic, materialInfo)
                                        materialDelegate?.refreshUI()
                                    })
                                }
                            }
                        }
                    }
                }
            }
            override fun onCancelled(snapshots: DatabaseError) {
            }
        })
    }
    fun stopLoading(){
            videoQuery.removeEventListener(videoValueEventListener)
            materialQuery.removeEventListener(materialValueEventListener)
    }*/

}

