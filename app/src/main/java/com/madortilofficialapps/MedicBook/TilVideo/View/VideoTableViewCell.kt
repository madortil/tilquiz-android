package com.madortilofficialapps.MedicBook.TilVideo.View

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.madortilofficialapps.MedicBook.R
import com.madortilofficialapps.MedicBook.TilQuizCode.model.TriviaQuestion
import com.madortilofficialapps.MedicBook.TilVideo.Model.LoadingProtocol
import com.madortilofficialapps.MedicBook.TilVideo.Model.Video
import com.madortilofficialapps.MedicBook.TilVideo.Model.VideoThumbnailImages
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.video_item.*

class VideoTableViewAdapter(private val videoCells: MutableList<Video>, private val searchString : String,
                            private val onClick: (Int) -> Unit): RecyclerView.Adapter<VideoTableViewAdapter.VideoTableViewHolder>(){

    var video : Video? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoTableViewHolder {
        return VideoTableViewHolder(LayoutInflater.from(parent.context).inflate(com.madortilofficialapps.MedicBook.R.layout.video_item, parent, false))
    }

    override fun getItemCount(): Int {
        return videoCells.size
    }

    override fun onBindViewHolder(holder: VideoTableViewHolder, position: Int) {
        video = videoCells[position]
        holder.bind(video, searchString, position, onClick)
    }

    override fun onViewRecycled(holder: VideoTableViewHolder) {
        super.onViewRecycled(holder)
        holder.stopAsyncTask()

    }
    override fun onViewAttachedToWindow(holder: VideoTableViewHolder) {
        super.onViewAttachedToWindow(holder)
    }
    class VideoTableViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer, LoadingProtocol {
        private var imageDownloader : DownloadImageTask? = null
        fun bind(video : Video?, searchString: String, position : Int, onClick : (Int) -> Unit){
            thumbNailButton.setImageResource(android.R.color.transparent)
            startLoading()
            if(video != null) {
                videoName.text = video!!.videoName
                thumbNailButton.setImageResource(R.drawable.default_thumbnail)
                if(!checkSavedVideo(video)){
                    imageDownloader = DownloadImageTask(thumbNailButton, this, video.Topic, video)
                    imageDownloader!!.execute(video.videoThumbnailURL)
                }
                if(searchString != ""){
                    if(video!!.videoName.contains(searchString)){
                        this.itemView.visibility = View.VISIBLE
                    }else{
                        this.itemView.visibility = View.GONE
                        this.itemView.setLayoutParams(RecyclerView.LayoutParams(this.itemView.width, 0))
                    }
                }else{
                    this.itemView.visibility = View.VISIBLE
                }
            }
            containerView.setOnClickListener {
                onClick(position)
            }
        }
        fun stopAsyncTask(){
            if(imageDownloader != null){
                imageDownloader!!.cancel(true)
            }
        }
        override fun finishLoading() {
            loadingPanel.visibility = View.GONE
            imageDownloader?.cancel(true)
        }

        override fun startLoading() {
            loadingPanel.visibility = View.VISIBLE
        }
        fun checkSavedVideo(video : Video) : Boolean{
            var savedVideoIndex = 0
            when(video.Topic){
                TriviaQuestion.Topic.trauma -> {
                    for(savedVideoThumbnail in VideoThumbnailImages.traumaThumbnailVideos){
                        if(savedVideoThumbnail == video){
                            thumbNailButton.setImageDrawable(VideoThumbnailImages.traumaThumbnails.get(savedVideoIndex))
                            finishLoading()
                            return true
                        }
                        savedVideoIndex++
                    }
                }
                TriviaQuestion.Topic.anamnesis -> {
                    for(savedVideoThumbnail in VideoThumbnailImages.anamnesisThumbnailVideos){
                        if(savedVideoThumbnail == video){
                            thumbNailButton.setImageDrawable(VideoThumbnailImages.anamnesisThumbnails.get(savedVideoIndex))
                            finishLoading()
                            return true
                        }
                        savedVideoIndex++
                    }
                }
                TriviaQuestion.Topic.anatomy -> {
                    for(savedVideoThumbnail in VideoThumbnailImages.anatomyThumbnailVideos){
                        if(savedVideoThumbnail == video){
                            thumbNailButton.setImageDrawable(VideoThumbnailImages.anatomyThumbnails.get(savedVideoIndex))
                            finishLoading()
                            return true
                        }
                        savedVideoIndex++
                    }
                }
                TriviaQuestion.Topic.medicine -> {
                    for(savedVideoThumbnail in VideoThumbnailImages.medicineThumbnailVideos){
                        if(savedVideoThumbnail == video){
                            thumbNailButton.setImageDrawable(VideoThumbnailImages.medicineThumbnails.get(savedVideoIndex))
                            finishLoading()
                            return true
                        }
                        savedVideoIndex++
                    }
                }
                TriviaQuestion.Topic.nbc -> {
                    for(savedVideoThumbnail in VideoThumbnailImages.nbcThumbnailVideos){
                        if(savedVideoThumbnail == video){
                            thumbNailButton.setImageDrawable(VideoThumbnailImages.nbcThumbnails.get(savedVideoIndex))
                            finishLoading()
                            return true
                        }
                        savedVideoIndex++
                    }
                }
                TriviaQuestion.Topic.cpr -> {
                    for(savedVideoThumbnail in VideoThumbnailImages.cprThumbnailVideos){
                        if(savedVideoThumbnail == video){
                            thumbNailButton.setImageDrawable(VideoThumbnailImages.cprThumbnails.get(savedVideoIndex))
                            finishLoading()
                            return true
                        }
                        savedVideoIndex++
                    }
                }
                TriviaQuestion.Topic.publicHealth -> {
                    for(savedVideoThumbnail in VideoThumbnailImages.publicHealthThumbnailVideos){
                        if(savedVideoThumbnail == video){
                            thumbNailButton.setImageDrawable(VideoThumbnailImages.publicHealthThumbnails.get(savedVideoIndex))
                            finishLoading()
                            return true
                        }
                        savedVideoIndex++
                    }
                }
                TriviaQuestion.Topic.mentalHealth -> {
                    for(savedVideoThumbnail in VideoThumbnailImages.mentalHealthThumbnailVideos){
                        if(savedVideoThumbnail == video){
                            thumbNailButton.setImageDrawable(VideoThumbnailImages.mentalHealthThumbnails.get(savedVideoIndex))
                            finishLoading()
                            return true
                        }
                        savedVideoIndex++
                    }
                }
                TriviaQuestion.Topic.teamWork -> {
                    for(savedVideoThumbnail in VideoThumbnailImages.teamWorkThumbnailVideos){
                        if(savedVideoThumbnail == video){
                            thumbNailButton.setImageDrawable(VideoThumbnailImages.teamWorkThumbnails.get(savedVideoIndex))
                            finishLoading()
                            return true
                        }
                        savedVideoIndex++
                    }
                }
                TriviaQuestion.Topic.routine -> {
                    for(savedVideoThumbnail in VideoThumbnailImages.routineThumbnailVideos){
                        if(savedVideoThumbnail == video){
                            thumbNailButton.setImageDrawable(VideoThumbnailImages.routineThumbnails.get(savedVideoIndex))
                            finishLoading()
                            return true
                        }
                        savedVideoIndex++
                    }
                }
                TriviaQuestion.Topic.other -> {
                    for(savedVideoThumbnail in VideoThumbnailImages.otherThumbnailVideos){
                        if(savedVideoThumbnail == video){
                            thumbNailButton.setImageDrawable(VideoThumbnailImages.otherThumbnails.get(savedVideoIndex))
                            finishLoading()
                            return true
                        }
                        savedVideoIndex++
                    }
                }
            }
            return false
        }
    }
    private class DownloadImageTask(internal var bmImage: ImageView, val delegate : LoadingProtocol, val topic : TriviaQuestion.Topic, val video : Video) : AsyncTask<String, Void, Bitmap>() {

        override fun doInBackground(vararg urls: String): Bitmap? {
            val urldisplay = urls[0]
            var mIcon11: Bitmap? = null
            try {
                val `in` = java.net.URL(urldisplay).openStream()
                mIcon11 = BitmapFactory.decodeStream(`in`)
            } catch (e: Exception) {
                Log.e("Error", e.message)
                e.printStackTrace()
            }
            return mIcon11
        }

        override fun onPostExecute(result: Bitmap?) {
            if(result != null){
                bmImage.setImageBitmap(result)
                when(topic){
                    TriviaQuestion.Topic.trauma -> {
                        VideoThumbnailImages.traumaThumbnails.add(bmImage.drawable)
                        VideoThumbnailImages.traumaThumbnailVideos.add(video)
                    }
                    TriviaQuestion.Topic.routine -> {
                        VideoThumbnailImages.routineThumbnails.add(bmImage.drawable)
                        VideoThumbnailImages.routineThumbnailVideos.add(video)
                    }
                    TriviaQuestion.Topic.teamWork -> {
                        VideoThumbnailImages.teamWorkThumbnails.add(bmImage.drawable)
                        VideoThumbnailImages.teamWorkThumbnailVideos.add(video)
                    }
                    TriviaQuestion.Topic.mentalHealth -> {
                        VideoThumbnailImages.mentalHealthThumbnails.add(bmImage.drawable)
                        VideoThumbnailImages.mentalHealthThumbnailVideos.add(video)
                    }
                    TriviaQuestion.Topic.publicHealth -> {
                        VideoThumbnailImages.publicHealthThumbnails.add(bmImage.drawable)
                        VideoThumbnailImages.publicHealthThumbnailVideos.add(video)
                    }
                    TriviaQuestion.Topic.cpr -> {
                        VideoThumbnailImages.cprThumbnails.add(bmImage.drawable)
                        VideoThumbnailImages.cprThumbnailVideos.add(video)
                    }
                    TriviaQuestion.Topic.nbc -> {
                        VideoThumbnailImages.nbcThumbnails.add(bmImage.drawable)
                        VideoThumbnailImages.nbcThumbnailVideos.add(video)
                    }
                    TriviaQuestion.Topic.medicine -> {
                        VideoThumbnailImages.medicineThumbnails.add(bmImage.drawable)
                        VideoThumbnailImages.medicineThumbnailVideos.add(video)
                    }
                    TriviaQuestion.Topic.anatomy -> {
                        VideoThumbnailImages.anatomyThumbnails.add(bmImage.drawable)
                        VideoThumbnailImages.anatomyThumbnailVideos.add(video)
                    }
                    TriviaQuestion.Topic.anamnesis -> {
                        VideoThumbnailImages.anamnesisThumbnails.add(bmImage.drawable)
                        VideoThumbnailImages.anamnesisThumbnailVideos.add(video)
                    }
                    TriviaQuestion.Topic.other -> {
                        VideoThumbnailImages.otherThumbnails.add(bmImage.drawable)
                        VideoThumbnailImages.otherThumbnailVideos.add(video)
                    }
                }
                delegate.finishLoading()
            }
        }
    }
}
