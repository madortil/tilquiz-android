package com.madortilofficialapps.MedicBook.TilVideo.Controller

import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.madortilofficialapps.MedicBook.R
import com.madortilofficialapps.MedicBook.TilVideo.Model.LoadingProtocol
import com.madortilofficialapps.MedicBook.TilVideo.Model.MenuBarDatabaseFacade
import com.madortilofficialapps.MedicBook.TilVideo.Model.Video
import com.madortilofficialapps.MedicBook.TilVideo.Model.refresher
import com.madortilofficialapps.MedicBook.TilVideo.View.VideoTableViewAdapter
import kotlinx.android.synthetic.main.fragment_video_table.*


class VideoTableViewController: Fragment(), refresher {

    var topic = MenuViewController.topicChosen
    private var Videos : MutableList<Video> = mutableListOf<Video>()
    var delegate : LoadingProtocol? = null
    var isRunning = true
    var searchString = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_video_table, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        topic = MenuViewController.topicChosen
        prepareVideoDatabase()
        videosRecyclerView.layoutManager = LinearLayoutManager(activity)
        videosRecyclerView.adapter = VideoTableViewAdapter(Videos, searchString) { position ->
            playVideo(position)
        }
        videoSearchBar.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                searchString = s.toString()
                reloadList()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
            }

        })
    }


    fun prepareVideoDatabase(){
        MenuBarDatabaseFacade.databaseSingleton.videoDelegate = this
        Videos = Video.getVideos(topic)
        if(!isLoaded()){
            //MenuBarDatabaseFacade.databaseSingleton.getVideos(topic)
        }
    }
    fun reloadList(){
        videosRecyclerView.layoutManager = LinearLayoutManager(activity)
        videosRecyclerView.adapter = VideoTableViewAdapter(Videos, searchString) { position ->
            playVideo(position)
        }
    }

    fun reloadView(){
        if(isRunning) {
            val ft = fragmentManager!!.beginTransaction()
            if (Build.VERSION.SDK_INT >= 26) {
                ft.setReorderingAllowed(false)
            }
            ft.detach(this).attach(this).commit()
        }
    }

    override fun refreshUI() {
        Videos = Video.getVideos(topic)
        for (video in Videos){
        }
        if (delegate != null && MenuViewController.tabSelected == Tabs.Video){
            delegate?.finishLoading()
        }
        reloadView()

    }
    //MARK play videos
    fun playVideo(position : Int)
    {
        val parent = parentFragment as MenuViewController
        if (Videos[position] != null){
            val selectedVideo = Videos[position]
            parent.openVideo(selectedVideo)
        }
    }

    //MARK Get functions
    fun isLoaded() : Boolean{
        return Videos.count() > 0
    }

    override fun onPause() {
        super.onPause()
        isRunning = false
    }

    override fun onResume() {
        super.onResume()
        isRunning = true
    }

}