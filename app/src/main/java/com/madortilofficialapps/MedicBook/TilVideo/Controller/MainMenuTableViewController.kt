package com.madortilofficialapps.MedicBook.TilVideo.Controller

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.madortilofficialapps.MedicBook.R
import com.madortilofficialapps.MedicBook.TilQuizCode.model.DatabaseFacade
import com.madortilofficialapps.MedicBook.TilQuizCode.model.TriviaQuestion
import com.madortilofficialapps.MedicBook.TilVideo.Model.MenuBarDatabaseFacade
import com.madortilofficialapps.MedicBook.TilVideo.View.MainMenuTableViewAdapter
import kotlinx.android.synthetic.main.fragment_main_menu.*

class MainMenuTableViewController: Fragment() {

    val Topics = arrayOf<TriviaQuestion.Topic>(TriviaQuestion.Topic.anatomy, TriviaQuestion.Topic.cpr, TriviaQuestion.Topic.medicine, TriviaQuestion.Topic.anamnesis,
            TriviaQuestion.Topic.routine, TriviaQuestion.Topic.trauma, TriviaQuestion.Topic.teamWork, TriviaQuestion.Topic.nbc, TriviaQuestion.Topic.mentalHealth, TriviaQuestion.Topic.publicHealth, TriviaQuestion.Topic.other)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        DatabaseFacade.fetchTriviaQuestions()
        MenuBarDatabaseFacade.databaseSingleton.getVideosAndMaterials()
        return inflater.inflate(R.layout.fragment_main_menu, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mainMenuTopicsRecyclerView.layoutManager = LinearLayoutManager(activity)
        mainMenuTopicsRecyclerView.adapter = MainMenuTableViewAdapter(Topics) { position ->
            MenuViewController.tabSelected = Tabs.Pdf
            MenuViewController.topicChosen = Topics[position]
            findNavController().navigate(R.id.action_MainMenuFragment_to_MenuViewFragment)
        }
    }
}